package im.ene.lab.wantedly.fragments;

import im.ene.lab.wantedly.widgets.TabHolderScrollingContent;
import android.support.v4.app.Fragment;

public abstract class ResumeBaseFragment extends Fragment implements TabHolderScrollingContent {

	public ResumeBaseFragment() {
		
	}
	
	@Override
	public void adjustScroll(int tabBarTop, int maxSroll) {
		
	}

}
